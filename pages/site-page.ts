import { type Page, type Locator } from "@playwright/test";

class sitePage {
  readonly page: Page;
  readonly siteMenu: Locator;
  readonly popUpError: Locator;

  constructor(page: Page) {
    this.page = page;
    this.siteMenu = page.getByLabel("Site");
  }

  async selectSite(sitename: string) {
    await this.siteMenu.selectOption(sitename);
  }

  async selectHospitalSite(sitename: string) {
    await this.selectSite(sitename);
  }

  async acceptDialog() {
    this.page.on("dialog", async (dialog) => {
      await dialog.accept();
    });
  }
}
export default sitePage;
