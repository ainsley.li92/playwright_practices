import { type Page, type Locator, expect } from "@playwright/test";

class hospitalPage {
  readonly page: Page;
  readonly topMenuBar: Locator;
  readonly siteLogo: Locator;
  readonly hospitalStats: Locator;

  constructor(page: Page) {
    this.page = page;
    this.topMenuBar = page.locator(".ui-dtl-top-bar");
    this.siteLogo = page.locator("#img-logo-main");
    this.hospitalStats = page.locator(".hospitals-stats");
  }

  async checkLoggedIn() {
    await expect(this.page).toHaveURL(/.*hospitals/);
    await expect(this.siteLogo).toBeVisible();
    await expect(this.topMenuBar).toBeVisible();
    await expect(this.hospitalStats).toBeVisible();
  }
}

export default hospitalPage;
