import { type Page, type Locator, expect } from "@playwright/test";
const hoursDropdownSelectionArray = [
  "01",
  "02",
  "03",
  "04",
  "05",
  "06",
  "07",
  "08",
  "09",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  "16",
  "17",
  "18",
  "19",
  "20",
  "21",
  "22",
  "23",
  "24",
];

const minutesDropdownSelectionArray = ["00", "15", "30", "45"];

class physicianHomePage {
  readonly page: Page;
  readonly logHours: Locator;
  readonly submit: Locator;
  readonly messages: Locator;
  readonly dutyMenu: Locator;
  readonly detailMenu: Locator;
  readonly dutyDescription: Locator;
  readonly datePicker: Locator;
  readonly hoursPicker: Locator;
  readonly minutesPicker: Locator;
  readonly calendar: Locator;
  readonly closeCalendarBtn: Locator;
  readonly homeIcon: Locator;

  constructor(page: Page) {
    this.page = page;
    this.logHours = page.locator("#button-log-hours-home");
    this.dutyMenu = page.getByLabel("Duty");
    this.detailMenu = page.locator("#button-duty-detail");
    this.dutyDescription = page.locator(".text no-select");
    this.datePicker = page.locator("#frm_duty_date");
    this.hoursPicker = page.locator("#frm_duty_hours");
    this.calendar = page.locator("#pickadate-container");
    this.closeCalendarBtn = page.locator(".picker__button--close");
    this.minutesPicker = page.locator("#frm_duty_minutes");
    this.homeIcon = page.locator("div#button-contract");
  }

  async clickDetailsBtn() {
    await this.logHours.click();
    await this.dutyMenu.selectOption("Business Development");
    await this.detailMenu.click();
  }

  async checkDutyDescription() {
    await expect(this.page).toHaveURL(/physicians\/index\/duty_detail/);
  }

  async clickHomeIcon() {
    this.homeIcon.click();
  }

  async openDatePicker() {
    await this.logHours.click();
    await this.datePicker.click();
    await expect(this.calendar).toBeEnabled();
    await this.closeCalendarBtn.click();
  }

  async clickHoursDropdown() {
    await this.hoursPicker.click();
  }

  async clickMinutesDropdown() {
    await this.minutesPicker.click();
  }

  async verifyHoursDropdownMenuOptions() {
    const hoursDropDownText = await this.hoursPicker.textContent();

    // Add a null check
    if (hoursDropDownText !== null) {
      const newValue = hoursDropDownText
        .trim()
        .split("\n")
        .filter(Boolean)
        .map((item) => item.trim());

      expect(newValue).toEqual(
        expect.arrayContaining(hoursDropdownSelectionArray),
      );
    } else {
      // Handle the case when hoursDropDownText is null
      console.error("hoursDropDownText is null");
    }
  }

  async verifyMinutesDropdownMenuOptions() {
    const extractedMinuteOptions = await this.minutesPicker.textContent();

    // Check if extractedMinuteOptions is not null
    if (extractedMinuteOptions !== null) {
      const minuteOptionsArray = extractedMinuteOptions
        .split("\n")
        .map((item) => item.trim()) // Trim whitespaces
        .filter(Boolean); // Remove empty strings

      expect(minuteOptionsArray).toEqual(
        expect.arrayContaining(minutesDropdownSelectionArray),
      );
    } else {
      console.error("extractedMinuteOptions is null");
    }
  }
}

export default physicianHomePage;
