import { type Page, type Locator } from "@playwright/test";

class LoginPage {
  readonly page: Page;
  readonly loginButton: Locator;
  readonly usernameField: Locator;
  readonly passwordField: Locator;

  constructor(page: Page) {
    this.page = page;
    this.usernameField = page.getByLabel("Username");
    this.passwordField = page.getByLabel("Password");
    this.loginButton = page.locator("#form_button_login");
  }

  async fillUsername(username: string) {
    await this.usernameField.fill(username);
  }

  async fillPassword(pasword: string) {
    await this.passwordField.fill(pasword);
  }

  async doLogin(username: string, password: string) {
    await this.fillUsername(username);
    await this.fillPassword(password);
    await this.loginButton.click();
  }
}

export default LoginPage;
