import { defineConfig, devices } from "@playwright/test";

require("dotenv").config(); // eslint-disable-line @typescript-eslint/no-var-requires

export default defineConfig({
  testDir: "./tests",
  /* Run tests in files in parallel */
  fullyParallel: true,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,
  /* Retry on CI only */
  retries: process.env.CI ? 2 : 0,
  /* Opt out of parallel tests on CI. */
  workers: process.env.CI ? 1 : undefined,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: "html",
  use: {
    trace: "on-first-retry",
    baseURL:
      process.env.STAGING === "1"
        ? "https://stage.ludidoctimelog.com/"
        : "https://qa.doctimelog.com/",
  },
  projects: [
    {
      name: "setup",
      testMatch: /.*\.setup\.ts/,
      // testIgnore: /auth\.setup\.ts/,
    },
    {
      name: "Google Chrome",
      dependencies: ["setup"],
      use: {
        ...devices["Desktop Chrome"],
        channel: "chrome",
        // Use prepared auth state.
        storageState: "playwright/.auth/",
      },
    },
  ],
});
