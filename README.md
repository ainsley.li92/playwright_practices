# DocTime Automation Repo
This repo is the QA automation suite for DocTime suites using Playwright framework.

## What is Playwright
Playwright Test was created specifically to accommodate the needs of end-to-end testing. Playwright supports all modern rendering engines including Google Chrome, Chromium, WebKit, and Firefox. Test on Windows, Linux, and macOS, locally or on CI, headless or headed with native mobile emulation of Google Chrome for Android and Mobile Safari.

## Getting started
1. Clone this repo locally
2. Set up NPM 
To set up npm (Node Package Manager), you need to install Node.js on your computer. Here are the steps to install Node.js and npm:

1. Go to the Node.js website (https://nodejs.org/) and click on the "Download" button.
1. Click on the version of Node.js that you want to download. Playwright can run with the latest versions.
1. Once the download is complete, open the installer and follow the prompts to install Node.js.
1. Once the installation is complete, open a terminal or command prompt and type `npm -v` to verify that npm has been installed and to see the version number.

## Install Playwright 
First, you will need to install Playwright on your machine. You can do this by running 

    # install latest version of playwright
    npm install -D @playwright/test@latest
    # install browsers
    npx playwright install

## Running Tests
    # headless mode 
    npx playwright test

    # headed mode (browser renders)
    npx playwright test --headed

    # html test report 
    npx playwright show-report

    # running the test in UI Mode
    npx playwright test --ui

    # run a single test 
    npx playwright test landing-page.spec.ts


## Support
https://playwright.dev/docs/intro

Playwright comes with its own vs code extension for running tests and debugging. Check out the [extension](https://marketplace.visualstudio.com/items?itemName=ms-playwright.playwright) for more information

## Debugging 

To debug one test file, run the Playwright test command with the name of the test file that you want to debug followed by the --debug flag.

```
npx playwright test example.spec.ts --debug
```

To debug a specific test from the line number where the test(.. is defined, add a colon followed by the line number at the end of the test file name, followed by the --debug flag.

```
npx playwright test example.spec.ts:10 --debug
```


