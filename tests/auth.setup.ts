import { test as setup, type Page } from "@playwright/test";
import pages from "../utils/pages";
import LoginPage from "../pages/login-page";
import SitePage from "../pages/site-page";

setup("authenticate as client admin", async ({ page }) => {
  const userName = process.env.USERNAME_CA!;
  const password = process.env.PASSWORD_CA!;
  await loginSteps(page, userName, password);
  await page.context().storageState({ path: "playwright/.auth/ca.json" });
});

setup("authenticate as provider", async ({ page }) => {
  const userName = process.env.USERNAME!;
  const password = process.env.PASSWORD!;
  await loginSteps(page, userName, password);
  await page.context().storageState({ path: "playwright/.auth/provider.json" });
});

async function loginSteps(page: Page, userName: string, password: string) {
  const baseURL = setup.info().project.use.baseURL!;
  const loginPage = new LoginPage(page);
  const sitePage = new SitePage(page);

  await page.goto(baseURL! + pages.loginPage);
  await loginPage.doLogin(userName, password);
  await sitePage.selectHospitalSite("Mount Simplified West");
}
