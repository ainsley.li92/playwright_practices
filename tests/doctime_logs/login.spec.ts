import { test } from "@playwright/test";
import pages from "../../utils/pages";
import LoginPage from "../../pages/login-page";
import SitePage from "../../pages/site-page";
import HospitalPage from "../../pages/hospitals-page";

const userName = process.env.USERNAME_CA!;
const password = process.env.PASSWORD_CA!;
let loginPage: LoginPage;
let sitePage: SitePage;
let hospitalPage: HospitalPage;

test.use({ storageState: { cookies: [], origins: [] } });

test.beforeEach(async ({ page }) => {
  await page.goto(pages.loginPage);
  loginPage = new LoginPage(page);
  sitePage = new SitePage(page);
  hospitalPage = new HospitalPage(page);
});

test.describe("DocTime Log - CA Login", () => {
  test(`successful login`, async () => {
    await loginPage.doLogin(userName, password);
    await sitePage.selectHospitalSite("Mount Simplified West");
    await hospitalPage.checkLoggedIn();
  });
});
