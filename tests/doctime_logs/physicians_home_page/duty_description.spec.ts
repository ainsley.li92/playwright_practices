import { test } from "@playwright/test";
import PhysiciansHomePage from "../../../pages/physicians-home-page";

let physiciansHomePage: PhysiciansHomePage;

test.use({ storageState: "playwright/.auth/provider.json" });

test.beforeEach(async ({ page }) => {
  await page.goto("/");
  await page.getByLabel("Site").selectOption("Mount Simplified West");
  physiciansHomePage = new PhysiciansHomePage(page);
});

test.describe("When logging hours", () => {
  test("verify duty descriptions are rendering correctly", async () => {
    await physiciansHomePage.clickDetailsBtn();
    await physiciansHomePage.checkDutyDescription();
  });

  test("verify date picker, hours and minutes dropdown menu opens and renders", async () => {
    await physiciansHomePage.openDatePicker();
    await physiciansHomePage.clickHoursDropdown();
    await physiciansHomePage.verifyHoursDropdownMenuOptions();
    await physiciansHomePage.clickMinutesDropdown();
    await physiciansHomePage.verifyMinutesDropdownMenuOptions();
  });
});
