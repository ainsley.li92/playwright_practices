import { test } from "@playwright/test";
// import PhysiciansHomePage from "../../../pages/physicians-home-page";

// let physiciansHomePage: PhysiciansHomePage;

test.use({ storageState: "playwright/.auth/provider.json" });

test.beforeEach(async ({ page }) => {
  await page.goto("/");
  await page.getByLabel("Site").selectOption("Mount Simplified West");
  // physiciansHomePage = new PhysiciansHomePage(page);
});

test.describe("When testing top menu items", () => {
  test("clicking home button refreshes on the same page", async () => {
    // await physiciansHomePage.clickHomeIcon();
  });

  test("verify date picker, hours and minutes dropdown menu opens and renders", async () => {});
});
