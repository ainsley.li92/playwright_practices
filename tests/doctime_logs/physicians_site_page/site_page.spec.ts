import { test } from "@playwright/test";
import SitePage from "../../../pages/site-page";

let sitePage: SitePage;

test.use({ storageState: "playwright/.auth/provider.json" });

test.beforeEach(async ({ page }) => {
  await page.goto("/");
  sitePage = new SitePage(page);
});

test.describe("When selecting site without contract", () => {
  test("verify correct popup message", async () => {
    await sitePage.selectHospitalSite("Mount Simplified East");
    await sitePage.acceptDialog();
  });
});
